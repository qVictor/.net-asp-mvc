﻿using BookingAppStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingAppStore.Controllers
{
    public class HomeController : Controller
    {
        BookContext db = new BookContext();
        UserContext dbUser = new UserContext();
        public ActionResult Index()
        {
            IEnumerable<Book> books = db.Books;
            IEnumerable<User> users = dbUser.Users;
            IEnumerable<Purchase> purchases = db.Purchases;
            ViewBag.Books = books;
            ViewBag.Purchases = purchases;
            ViewBag.Users = users;            
            return View();
        }

        public string Info()
        {
            HttpContext.Response.Write("<h2>Информация о пользователе</h2>");

            string user_agent = HttpContext.Request.UserAgent;
            string browser = Request.Browser.Browser;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            bool IsAuth = HttpContext.User.Identity.IsAuthenticated;
            string login = HttpContext.User.Identity.Name;

            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            return "<p>Браузер - " + browser + "</p><p>url - " + url + "</p><p>ip - " + ip + "</p><p>Авторизация пользователя - " + IsAuth + "</p><p>Логин - " + login;
        }
        public ActionResult GetList()
        {
            ViewBag.Massage = "partical view";
            return PartialView();
        }
        public FileResult GetFile()
        {
            string file_path = Server.MapPath("~/Files/Info.txt");
            string file_type = "application/txt";
            string file_name = "Info.txt";
            return File(file_path, file_type, file_name);
        }



        [HttpGet]
        public ActionResult Buy(int id)
        {
            ViewBag.BookId = id;
            return View();
        }
        [HttpPost]
        public ActionResult Buy(Purchase purchase)
        {
            if (ModelState.IsValid)
            {
                purchase.Date = DateTime.Now;
                //var BookName = db.Books.Include(p => p.Name);
                db.Purchases.Add(purchase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Book book)
        {
            db.Books.Add(book); //insert
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Book b = db.Books.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            return View(b);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Book b = db.Books.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            db.Books.Remove(b);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Book b = db.Books.Find(id);

            if (b == null)
            {
                return HttpNotFound();
            }
            return View(b);
        }
        [HttpPost]
        public ActionResult Edit(Book b)
        {
            db.Entry(b).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult CreateUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(User user)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");                
            }
            return View();
        }
        
    }
}