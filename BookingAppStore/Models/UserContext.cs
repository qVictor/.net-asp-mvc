﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingAppStore.Models
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
    public class UserInitializer : DropCreateDatabaseAlways<UserContext>
    {
        protected override void Seed(UserContext db)
        {
            db.Users.Add(new User { Name = "name1", YearOfBirth = 1996 , MonthBirth = 100, DayBirth = 20 });
            base.Seed(db);

        }
    }
}