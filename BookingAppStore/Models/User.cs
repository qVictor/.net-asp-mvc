﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookingAppStore.Models
{
    public class User
    {
        public int Id { get; set; }
        [Display(Name = "Имя")]
        [Required(ErrorMessage ="Поле не должно быть пустым")]
        public string Name { get; set; }

        [Display(Name = "Год рождения")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Range(1900, 2016, ErrorMessage = "Недопустимое значение, введите значение в промежутке от 1900 до 1016")]
        public int YearOfBirth { get; set; }

        [Display(Name = "Месяц рождения")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Range(1, 12, ErrorMessage = "Недопустимое значение, введите значение в промежутке от 1 до 12")]
        public int MonthBirth { get; set; }

        [Display(Name = "День рождения")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Range(1, 31, ErrorMessage = "Недопустимое значение, введите значение в промежутке от 1 до 31")]
        public int DayBirth { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [RegularExpression(@"^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$", ErrorMessage ="Некорректный адрес") ]
        public string Email { get; set; }

        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        public string Password { get; set; }

        [Display(Name = "Повтор пароля")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string PasswordConfirm { get; set; }

    }
}