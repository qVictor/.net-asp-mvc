﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BookingAppStore.Models
{
    public class BookContext : DbContext
    {
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<Book> Books { get; set; }
    }
    public class BookInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext db)
        {
            db.Books.Add(new Book { Name = "name1", Author = "author1", Price = 100});
            db.Books.Add(new Book { Name = "name2", Author = "author2", Price = 200 });
            db.Books.Add(new Book { Name = "name3", Author = "author3", Price = 300 });
            //DateTime date1 = new DateTime();
            //db.Purchases.Add(new Purchase { Person = "name1", Address = "address", BookId = 1, Date = date1 });
            base.Seed(db);

        }
    }
}